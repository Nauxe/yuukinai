﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace Yukinai
{
    class Program
    {
        public static List<string> rgbmap = new List<string> { };
        public static List<string> colmap = new List<string> { };


        static void Main(string[] args)
        {
            Serializer serial = new Serializer();
                rgbmap = serial.DeSerializeObject<List<string>>("rgbmap.bin");
                colmap = serial.DeSerializeObject<List<string>>("colmap.bin");
            var image = "cat.bmp";
            Bitmap frame = new Bitmap(image);
            Console.WriteLine("Analysis for {0}", image);
            Console.WriteLine("--H: {0}  --Y:{1}", frame.Height, frame.Width);
            Console.WriteLine("R{0} G{1} B{2} :: {3}", frame.GetPixel(0, 0).R, frame.GetPixel(0, 0).G, frame.GetPixel(0, 0).B, getColorNameRef(frame.GetPixel(0, 0).ToString()));
            for (int x = 0; x < frame.Height; x++)
            {
                for (int y = 0; y < frame.Width; y++)
                {
                    Console.BackgroundColor = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), getColorNameRef(frame.GetPixel(y, x).ToString()));
                    Console.Write(ColorTranslator.ToHtml(frame.GetPixel(y, x)));
                }
                Console.Write("\n");
            }

            for (int x = 0; x < frame.Height; x++)
            {
                for (int y = 0; y < frame.Width; y++)
                {
                    Console.BackgroundColor = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), getColorNameRef(frame.GetPixel(y, x).ToString()));
                    Console.Write(" ");
                }
                Console.Write("\n");
            }
        }

        public static string getColorNameRef(string rgb)
        {
            Serializer serial = new Serializer();
            if (rgbmap.IndexOf(rgb) != -1)
            {
                return colmap[rgbmap.IndexOf(rgb)];
            }
            else
            {
                Console.Write("No Colormap found for {0}. Enter Colormap: ", rgb); colmap.Add(Console.ReadLine());
                rgbmap.Add(rgb);
                serial.SerializeObject(rgbmap, "rgbmap.bin");
                serial.SerializeObject(colmap, "colmap.bin");
                return colmap[colmap.Count - 1];
            }
        }

    }

}

class Serializer { 
        /// <summary>
        /// Serializes an object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializableObject"></param>
        /// <param name="fileName"></param>
        public void SerializeObject<T>(T serializableObject, string fileName)
        {
            if (serializableObject == null) { return; }

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                XmlSerializer serializer = new XmlSerializer(serializableObject.GetType());
                using (MemoryStream stream = new MemoryStream())
                {
                    serializer.Serialize(stream, serializableObject);
                    stream.Position = 0;
                    xmlDocument.Load(stream);
                    xmlDocument.Save(fileName);
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                //Log exception here
            }
        }


        /// <summary>
        /// Deserializes an xml file into an object list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public T DeSerializeObject<T>(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) { return default(T); }

            T objectOut = default(T);

            try
            {
                string attributeXml = string.Empty;

                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                string xmlString = xmlDocument.OuterXml;

                using (StringReader read = new StringReader(xmlString))
                {
                    Type outType = typeof(T);

                    XmlSerializer serializer = new XmlSerializer(outType);
                    using (XmlReader reader = new XmlTextReader(read))
                    {
                        objectOut = (T)serializer.Deserialize(reader);
                        reader.Close();
                    }

                    read.Close();
                }
            }
            catch (Exception ex)
            {
                //Log exception here
            }

            return objectOut;
        }



}
